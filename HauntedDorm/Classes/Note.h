#ifndef __NOTE_H__
#define __NOTE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class Note : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene(std::vector<int> item1);

	virtual bool init();
	void getItem(std::vector<int> item1);
	bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);
	void onClickMenuItem(cocos2d::Ref* sender);
	std::vector<int> giveItem();

	CREATE_FUNC(Note);

private:
	std::vector<int> item;
	cocos2d::Sprite *BackButton;
	int onHand;
};

#endif // __NOTE_H__
