#include "Note.h"
#include "Definitions.h"
#include "GameScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

std::vector<int> item3;
std::string des_JSON = "", title_form_JSON = "";
int childTagTit = 0, childTagNote = 0, childTagCon = 0;

Scene* Note::createScene(std::vector<int> item1)
{
	for (int i = 0; i < item1.size(); i++)
		item3.push_back(item1[i]);
	return Note::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool Note::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//Scroll View List Of Item
	cocos2d::ui::ScrollView *scrollView = cocos2d::ui::ScrollView::create();
	scrollView->setDirection(cocos2d::ui::ScrollView::Direction::VERTICAL);
	scrollView->setContentSize(Size(300, 400));
	scrollView->setInnerContainerSize(Size(1280, 2500));
	scrollView->setBackGroundColor(Color3B::GREEN);
	scrollView->setBounceEnabled(true);
	scrollView->setAnchorPoint(Vec2(0.5, 0.5));
	scrollView->setPosition(Vec2(250, 300));

	auto ItemList = Label::createWithTTF(StringUtils::format("List of Note"), "fonts/Spooky Halloween.ttf", visibleSize.height * SPLASH_FONT_SIZE);
	ItemList->setColor(Color3B::GREEN);
	ItemList->setPosition(Vec2(250, 500 + visibleSize.height * SPLASH_FONT_SIZE));
	this->addChild(ItemList);

	float position = 2500;

	for (int i = 0; i < item3.size(); i++) {
		cocos2d::ui::Button *button;
		if (item3[i] == 1) {
			button = cocos2d::ui::Button::create("Sprites/Game Scene/Note Scene/List.png", "Sprites/Game Scene/Note Scene/List.png");
			button->setTitleText("key");
			button->setTitleFontName("fonts/arial.ttf");
			button->setTitleFontSize(50.0f);
			button->setScale(0.5);
			button->setPosition(Vec2(scrollView->getContentSize().width / 2, position - 40));
			position -= 40 * 2;
			scrollView->addChild(button);
			button->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type) {
				switch (type)
				{
				case ui::Widget::TouchEventType::ENDED:
					des_JSON = "key";
					title_form_JSON = "key";
					break;
				default:
					break;
				}

				//Background Newspaper
				auto newsPaper = Sprite::create("Sprites/Game Scene/Note Scene/Newspaper.jpg");
				newsPaper->setPosition(Point(750, 400));

				//Title Newspaper
				auto Tit = Label::createWithTTF(title_form_JSON, "fonts/Spooky Halloween.ttf", 50);
				Tit->setColor(Color3B::BLUE);
				Tit->setPosition(Point(750, 350));

				//Content Newspaper
				auto Cont = Label::createWithTTF(des_JSON, "fonts/Spooky Halloween.ttf", 50);
				Cont->setColor(Color3B::BLUE);
				Cont->setPosition(Point(750, 300));

				//Change child
				newsPaper->setTag(1);
				Tit->setTag(2);
				Cont->setTag(3);
				this->removeChildByTag(childTagTit);
				this->removeChildByTag(childTagNote);
				this->removeChildByTag(childTagCon);
				this->addChild(newsPaper);
				this->addChild(Tit);
				this->addChild(Cont);
				childTagTit = 1;
				childTagNote = 2;
				childTagCon = 3;
			});
		} else 
		if (item3[i] == 2) {
			button = cocos2d::ui::Button::create("Sprites/Game Scene/Note Scene/List.png", "Sprites/Game Scene/Note Scene/List.png");
			button->setTitleText("rice");
			button->setTitleFontName("fonts/arial.ttf");
			button->setTitleFontSize(50.0f);
			button->setScale(0.5);
			button->setPosition(Vec2(scrollView->getContentSize().width / 2, position - 47));
			position -= 47 * 2;
			scrollView->addChild(button);
			button->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type) {
				switch (type)
				{
				case ui::Widget::TouchEventType::ENDED:
					des_JSON = "rice";
					title_form_JSON = "rice";
					break;
				default:
					break;
				}

				//Background PaperNote
				auto newsPaper = Sprite::create("Sprites/Game Scene/Note Scene/PaperNote.png");
				newsPaper->setPosition(Point(750, 400));

				//Title PaperNote
				auto Tit = Label::createWithTTF(title_form_JSON, "fonts/Spooky Halloween.ttf", 50);
				Tit->setColor(Color3B::BLUE);
				Tit->setPosition(Point(750, 350));

				//Content PaperNote
				auto Cont = Label::createWithTTF(des_JSON, "fonts/Spooky Halloween.ttf", 50);
				Cont->setColor(Color3B::BLUE);
				Cont->setPosition(Point(750, 300));

				//Change child
				newsPaper->setTag(4);
				Tit->setTag(5);
				Cont->setTag(6);
				this->removeChildByTag(childTagTit);
				this->removeChildByTag(childTagNote);
				this->removeChildByTag(childTagCon);
				this->addChild(newsPaper, 0);
				this->addChild(Tit);
				this->addChild(Cont);
				childTagTit = 4;
				childTagNote = 5;
				childTagCon = 6;
			});
		}
	}

	this->addChild(scrollView);

	//Back
	BackButton = Sprite::create("Sprites/Game Scene/Inventory Scene/back.png");
	BackButton->setPosition(Point(visibleSize.width / 3, 100));
	this->addChild(BackButton);

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(Note::onTouchBegan, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);

	this->scheduleUpdate();

	return true;
}

bool Note::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event) {
	Point touchLocation = touch->getLocation();
	Point BackLocation = BackButton->getPosition();

	if (BackLocation.x - 124 <= touchLocation.x && touchLocation.x <= BackLocation.x + 124) {
		if (BackLocation.y - 34 <= touchLocation.y && touchLocation.y <= BackLocation.y + 34) {
			item.clear();
			Director::getInstance()->popScene();
		}
	}

	return true;
}

void Note::getItem(std::vector<int> item1) {
	for (int i = 0; i < item1.size(); i++) {
		item.push_back(item1[i]);
	}
}

std::vector<int> Note::giveItem() {
	return item;
}
