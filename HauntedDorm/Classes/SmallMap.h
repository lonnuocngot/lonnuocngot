#ifndef __SMALL_MAP_H__
#define __SMALL_MAP_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class SmallMap : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene(std::vector<int> item1);

	virtual bool init();
	void getItem(std::vector<int> item1);
	bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);
	void onClickMenuItem(cocos2d::Ref* sender);
	std::vector<int> giveItem();

	CREATE_FUNC(SmallMap);

private:
	std::vector<int> item;
	cocos2d::Sprite *BackButton, *SMap;
	int onHand;
};

#endif // __SMALL_MAP_H__
