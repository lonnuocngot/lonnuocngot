#ifndef __CABINET_H__
#define __CABINET_H__

#include "cocos2d.h"

class Cabinet : public cocos2d::Scene
{
public:

	Cabinet(cocos2d::Scene *scene, float posX, float posY);
	std::vector<int> onLoot();
	void getItem(std::vector<int> item1);
	void fixPosition(float posX, float posY);
	float getPositionX();
	float getPositionY();
	void clearCabinet();

private:
	cocos2d::Size visibleSize;
	cocos2d::Vec2 origin;

	cocos2d::Sprite *cabinet1;

	std::vector<int> item;
};

#endif // __CABINET_H__
