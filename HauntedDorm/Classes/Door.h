#ifndef __DOOR_H__
#define __DOOR_H__

#include "cocos2d.h"

class Door : public cocos2d::Scene
{
public:

	Door(cocos2d::Scene *scene, float posX, float posY);
	void fixPosition(float posX, float posY);
	float getPositionX();
	float getPositionY();

private:
	cocos2d::Size visibleSize;
	cocos2d::Vec2 origin;

	cocos2d::Sprite *door1;
};

#endif // __DOOR_H__
