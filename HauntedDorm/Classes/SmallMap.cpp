#include "SmallMap.h"
#include "Definitions.h"
#include "GameScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

std::vector<int> item4;

Scene* SmallMap::createScene(std::vector<int> item1)
{
	for (int i = 0; i < item1.size(); i++)
		item4.push_back(item1[i]);
	return SmallMap::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool SmallMap::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//Map
	SMap = Sprite::create("Sprites/Game Scene/Map Scene/map.jpg");
	SMap->setScale(4);
	SMap->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	this->addChild(SMap);

	//Back
	BackButton = Sprite::create("Sprites/Game Scene/Inventory Scene/back.png");
	BackButton->setPosition(Point(visibleSize.width / 3, 100));
	this->addChild(BackButton);

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(SmallMap::onTouchBegan, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);

	this->scheduleUpdate();

	return true;
}

bool SmallMap::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event) {
	Point touchLocation = touch->getLocation();
	Point BackLocation = BackButton->getPosition();

	if (BackLocation.x - 124 <= touchLocation.x && touchLocation.x <= BackLocation.x + 124) {
		if (BackLocation.y - 34 <= touchLocation.y && touchLocation.y <= BackLocation.y + 34) {
			item.clear();
			Director::getInstance()->popScene();
		}
	}

	return true;
}

void SmallMap::getItem(std::vector<int> item1) {
	for (int i = 0; i < item1.size(); i++) {
		item.push_back(item1[i]);
	}
}

std::vector<int> SmallMap::giveItem() {
	return item;
}
