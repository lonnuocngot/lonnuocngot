#ifndef __CLOSET_H__
#define __CLOSET_H__

#include "cocos2d.h"

class Closet : public cocos2d::Scene
{
public:

	Closet(cocos2d::Scene *scene, float posX, float posY);
	void fixPosition(float posX, float posY);
	float getPositionX();
	float getPositionY();

private:
	cocos2d::Size visibleSize;
	cocos2d::Vec2 origin;

	cocos2d::Sprite *closet;
};

#endif // __CLOSET_H__
