#include "MainMenuScene.h"
#include "Definitions.h"
#include "GameScene.h"

USING_NS_CC;

Scene* MainMenuScene::createScene()
{
    return MainMenuScene::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool MainMenuScene::init()
{
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto backgroundSprite = Sprite::create("Sprites/Main Menu Scene/Background.png");
	backgroundSprite->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	this->addChild(backgroundSprite);

	auto gameName = Label::createWithTTF(StringUtils::format("HAUTED DORM"), "fonts/Spooky Halloween.ttf", visibleSize.height * SPLASH_FONT_SIZE);
	gameName->setColor(Color3B::GREEN);
	gameName->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height * 9 / 10 + origin.y));
	this->addChild(gameName);

	auto MenuItem1 = MenuItemImage::create("Sprites/Main Menu Scene/ContinueButton1.png", "Sprites/Main Menu Scene/ContinueButton2.png", CC_CALLBACK_1(MainMenuScene::onClickMenuItem, this));
	MenuItem1->setTag(1);

	auto MenuItem2 = MenuItemImage::create("Sprites/Main Menu Scene/PlayButton1.png", "Sprites/Main Menu Scene/PlayButton2.png", CC_CALLBACK_1(MainMenuScene::onClickMenuItem, this));
	MenuItem2->setTag(2);

	auto MenuItem3 = MenuItemImage::create("Sprites/Main Menu Scene/LoadButton1.png", "Sprites/Main Menu Scene/LoadButton2.png", CC_CALLBACK_1(MainMenuScene::onClickMenuItem, this));
	MenuItem3->setTag(3);

	auto MenuItem4 = MenuItemImage::create("Sprites/Main Menu Scene/OptionButton1.png", "Sprites/Main Menu Scene/OptionButton2.png", CC_CALLBACK_1(MainMenuScene::onClickMenuItem, this));
	MenuItem4->setTag(4);

	auto MenuItem5 = MenuItemImage::create("Sprites/Main Menu Scene/QuitButton1.png", "Sprites/Main Menu Scene/QuitButton2.png", CC_CALLBACK_1(MainMenuScene::onClickMenuItem, this));
	MenuItem5->setTag(5);

	auto menu = Menu::create(MenuItem1, MenuItem2, MenuItem3, MenuItem4, MenuItem5, nullptr);
	menu->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height * 4 / 10 + origin.y));
	menu->setScaleY(0.75f);
	menu->alignItemsVertically();

	addChild(menu);

	return true;
}


void MainMenuScene::onClickMenuItem(cocos2d::Ref* sender)
{
	auto node = dynamic_cast<Node*> (sender);

	CCLOG("%d", node->getTag());

	if (node->getTag() == 1) {
		// Continue Button
		// Load checkpoint nearest

	} else
	if (node->getTag() == 2) {
		// Play Button
		// Play New game
		auto scene = GameScene::createScene();
		Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
	} else
	if (node->getTag() == 3) {
		// Load Button
		// Load save scene

	} else
	if (node->getTag() == 4) {
		// Option Button
		// Setting Menu

	} else
	if (node->getTag() == 5) {
		// Quit Button
		// Quit Game
		CC_DIRECTOR_END();
	}
}
