#ifndef __INVENTORY_H__
#define __INVENTORY_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class Inventory : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene(std::vector<int> item1);

	virtual bool init();
	void getItem(std::vector<int> item1);
	bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);
	std::vector<int> giveItem();
	void onClickMenuItem(cocos2d::Ref* sender);
	int giveHand();

	CREATE_FUNC(Inventory);

private:
	std::vector<int> item;
	cocos2d::Sprite *BackButton, *Choose;
	int onHand;
};

#endif // __INVENTORY_H__
