#include "Closet.h"
#include "LoseScene.h"
#include "Definitions.h"

USING_NS_CC;

Closet::Closet(cocos2d::Scene *scene, float posX, float posY) {
	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();
	
	closet = Sprite::create("Sprites/Game Scene/Background Entity/closet.png");
	closet->setPosition(Point(posX, posY));

	scene->addChild(closet);
}

void Closet::fixPosition(float posX, float posY) {
	closet->setPosition(Point(posX, posY));
}

float Closet::getPositionX() {
	return closet->getPositionX();
}

float Closet::getPositionY() {
	return closet->getPositionY();
}