#include "Cabinet.h"
#include "Definitions.h"

USING_NS_CC;

Cabinet::Cabinet(cocos2d::Scene *scene, float posX, float posY) {
	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();
	
	cabinet1 = Sprite::create("Sprites/Game Scene/Background Entity/TULOOT.png");
	cabinet1->setPosition(Point(posX, posY));

	scene->addChild(cabinet1);
}

void Cabinet::getItem(std::vector<int> item1){
	for (int i = 0; i < item1.size(); i++) {
		item.push_back(item1[i]);
	}
}

std::vector<int> Cabinet::onLoot() {
	return item;
}

void Cabinet::fixPosition(float posX, float posY) {
	cabinet1->setPosition(Point(posX, posY));
}

float Cabinet::getPositionX() {
	return cabinet1->getPositionX();
}

float Cabinet::getPositionY() {
	return cabinet1->getPositionY();
}

void Cabinet::clearCabinet() {
	item.clear();
}