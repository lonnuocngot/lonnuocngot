#ifndef __DEFINITIONS_H__
#define __DEFINITIONS_H__

#define DISPLAY_TIME_SPLASH_SCENE 2
#define TRANSITION_TIME 0.5f
#define SPLASH_FONT_SIZE 0.1f

#define CHARACTER_SPEED 1
#define RUN_BOOST 9

#endif // __DEFINITIONS_H__
