#ifndef __LOSE_SCENE_H__
#define __LOSE_SCENE_H__

#include "cocos2d.h"

class LoseScene : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	// implement the "static create()" method manually
	CREATE_FUNC(LoseScene);

private:
	void GoToMainMenuScene(float dt);
};

#endif // __LOSE_SCENE_H__
