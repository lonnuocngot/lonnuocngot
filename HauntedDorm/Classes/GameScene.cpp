#include "GameScene.h"
#include "Definitions.h"
#include "Door.h"
#include "Cabinet.h"
#include "Inventory.h"
#include "LoseScene.h"
#include "Note.h"
#include "SmallMap.h"
#include "Closet.h"
#include "json/document.h"
#include "json/rapidjson.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

Scene* GameScene::createScene()
{
    return GameScene::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

//declare variable
Inventory *inventory = new Inventory();
Note *note = new Note();
SmallMap *smallMap = new SmallMap();
Cabinet *cabinet;
Door *door;
Closet *clos;
float isWalk = 0, runBoost = 0, speed = 0, diffX = 0, diffY = 0, staminaDec = 0;
bool outdoor = false;
cocos2d::Sprite *backgroundSprite, *InventorySprite, *MapSprite, *NoteSprite;

// on "init" you need to initialize your instance
bool GameScene::init()
{
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	backgroundSprite = Sprite::create("Sprites/Game Scene/Background Entity/maptest.png");
	backgroundSprite->setScale(2);
	backgroundSprite->setPosition(Point(5120, 1920));
	this->addChild(backgroundSprite, 0);

	//Heart bar
	auto BGHeartBar = cocos2d::ui::LoadingBar::create("Sprites/Game Scene/BackGround Entity/BGHeartBar.png");
	BGHeartBar->setPosition(Point(220, visibleSize.height - 25));
	BGHeartBar->setDirection(cocos2d::ui::LoadingBar::Direction::LEFT);
	BGHeartBar->setScaleX(0.75);
	BGHeartBar->setPercent(100);
	this->addChild(BGHeartBar, 5);
	HeartBar = cocos2d::ui::LoadingBar::create("Sprites/Game Scene/BackGround Entity/HeartBar.png");
	HeartBar->setDirection(cocos2d::ui::LoadingBar::Direction::LEFT);
	HeartBar->setPosition(Point(220, visibleSize.height - 25));
	HeartBar->setScaleX(0.75);
	HeartBar->setPercent(50);
	this->addChild(HeartBar, 6);

	//Stamina Bar
	StaminaBar = cocos2d::ui::LoadingBar::create("Sprites/Game Scene/BackGround Entity/StaminaBar.png");
	StaminaBar->setPosition(Point(220, visibleSize.height - 75));
	StaminaBar->setDirection(cocos2d::ui::LoadingBar::Direction::LEFT);
	StaminaBar->setScaleX(0.75);
	StaminaBar->setPercent(100);
	this->addChild(StaminaBar, 6);

	//Recovery

	mainCharacter = Sprite::create("Sprites/Game Scene/Main Character/MainChar.png");
	mainCharacter->setPosition(Point(100, 300));
	diffX = 100;
	diffY = 300;
	this->addChild(mainCharacter, 2);

	InventorySprite = Sprite::create("Sprites/Game Scene/Icon/BagIcon.png");
	InventorySprite->setPosition(Point(424, 50));
	InventorySprite->setScale(0.1);
	this->addChild(InventorySprite, 4);
	MapSprite = Sprite::create("Sprites/Game Scene/Icon/MapIcon.png");
	MapSprite->setPosition(Point(524, 50));
	MapSprite->setScale(0.1);
	this->addChild(MapSprite, 4);
	NoteSprite = Sprite::create("Sprites/Game Scene/Icon/NoteIcon.jpg");
	NoteSprite->setPosition(Point(624, 50));
	NoteSprite->setScale(0.1);
	this->addChild(NoteSprite, 4);

	//-----------------------------------------------------------------------------------------

	//item interact
	//Door
	//door = new Door(this, 300, 200);
	std::string str = FileUtils::getInstance()->getStringFromFile("res/database/MapInfo.json");

	rapidjson::Document doc;

	doc.Parse<0>(str.c_str());
	if (doc.HasParseError()) {
		CCLOG("Error !");
	}
	else {
		if (doc.HasMember("room")) {
			rapidjson::Value &dSV = doc["room"];
			for (rapidjson::SizeType i = 0; i < dSV.Size(); i++) {
				listOfDoor.push_back(Point(dSV[i][0].GetInt(), dSV[i][1].GetInt()));
				listOfTele.push_back(Point(dSV[i][2].GetInt(), dSV[i][3].GetInt()));
				listOfSizeDoor.push_back(Point(dSV[i][4].GetInt(), dSV[i][5].GetInt()));
				ChangeY.push_back(dSV[i][6].GetInt());
			}
		}
	}

	

	//Cabinet
	cabinet = new Cabinet(this, 900, 400);
	cabinet->getItem({1, 2, 3});
	listOfCabinet.push_back(Point(900, 400));

	//closet
	clos = new Closet(this, 1200, 400);
	listOfCloset.push_back(Point(1200, 400));
	
	//----------------------------------------------------------------------------------------

	//touch events
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(GameScene::onTouchBegan, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);

	// keyboard event
	auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(GameScene::onKeyPressed, this);
	listener->onKeyReleased = CC_CALLBACK_2(GameScene::onKeyReleased, this);

	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);

	this->scheduleUpdate();

	return true;
}

void GameScene::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
	CCLOG("Key with keycode %d pressed", keyCode);

	switch (keyCode) {
	case EventKeyboard::KeyCode::KEY_SHIFT:
		runBoost = 1.0f;
		break;
	case EventKeyboard::KeyCode::KEY_A:
		isWalk = -1.0f;
		break;
	case EventKeyboard::KeyCode::KEY_D:
		isWalk = +1.0f;
		break;
	case EventKeyboard::KeyCode::KEY_S:
		outdoor = true;
		mainCharacter->setVisible(true);
		break;
	}
}

void GameScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
	CCLOG("Key with keycode %d released", keyCode);

	switch (keyCode) {
	case EventKeyboard::KeyCode::KEY_A:
	case EventKeyboard::KeyCode::KEY_D:
		isWalk = 0.0f;
		break;
	case EventKeyboard::KeyCode::KEY_SHIFT:
		runBoost = 0.0f;
		break;
	case EventKeyboard::KeyCode::KEY_W:
		outdoor = true;
		mainCharacter->setVisible(true);
		break;
	}
}

bool GameScene::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event) {
	Point touchLocation = touch->getLocation();
	Point CharLocation = mainCharacter->getPosition();

	CCLOG("%f %f:", CharLocation.x, CharLocation.y);

	bool kt = false; // check inventory or map or note on click

	if (kt == false) {
		if (InventorySprite->getPositionX() - 43 <= touchLocation.x && touchLocation.x <= InventorySprite->getPositionX() + 43) {
			if (InventorySprite->getPositionY() - 52 <= touchLocation.y && touchLocation.y <= InventorySprite->getPositionY() + 52) {
				kt = true;

				auto scene = inventory->createScene(inventory->giveItem());

				Director::getInstance()->pushScene(scene);
			}
		}
	}
	if (kt == false) {
		if (NoteSprite->getPositionX() - 43 <= touchLocation.x && touchLocation.x <= NoteSprite->getPositionX() + 43) {
			if (NoteSprite->getPositionY() - 52 <= touchLocation.y && touchLocation.y <= NoteSprite->getPositionY() + 52) {
				kt = true;

				note->getItem({1, 2});

				auto scene = note->createScene(note->giveItem());

				Director::getInstance()->pushScene(scene);
			}
		}
	}
	if (kt == false) {
		if (MapSprite->getPositionX() - 43 <= touchLocation.x && touchLocation.x <= MapSprite->getPositionX() + 43) {
			if (MapSprite->getPositionY() - 52 <= touchLocation.y && touchLocation.y <= MapSprite->getPositionY() + 52) {
				kt = true;

				auto scene = smallMap->createScene(smallMap->giveItem());

				Director::getInstance()->pushScene(scene);
			}
		}
	}
	if (kt == false) {
		//check door
		for (int i = 0; i < listOfDoor.size(); i++) {

			float Sxlb = listOfDoor[i].x - listOfSizeDoor[i].x / 2 - diffX + CharLocation.x;
			float Sxub = listOfDoor[i].x + listOfSizeDoor[i].x / 2 - diffX + CharLocation.x;
			float Sylb = listOfDoor[i].y - listOfSizeDoor[i].y / 2 - diffY + CharLocation.y;
			float Syub = listOfDoor[i].y + listOfSizeDoor[i].y / 2 - diffY + CharLocation.y;

			if ((Sxlb <= CharLocation.x && CharLocation.x <= Sxub) && (Sylb <= CharLocation.y && CharLocation.y <= Syub)) {
				if ((Sxlb <= touchLocation.x && touchLocation.x <= Sxub) && (Sylb <= touchLocation.y && touchLocation.y <= Syub)) {
					backgroundSprite->setPosition(Point(5120 - listOfTele[i].x + CharLocation.x * 1.5, 1920 - listOfTele[i].y + CharLocation.y * 1.5));
					diffX = listOfTele[i].x;
					diffY = listOfTele[i].y;
					break;
				}
			}
		}

		CCLOG("%d", listOfCabinet.size());

		//check cabinet
		for (int i = 0; i < listOfCabinet.size(); i++) {
			float Sxlb = listOfCabinet[i].x - 161 - diffX + CharLocation.x;
			float Sxub = listOfCabinet[i].x + 161 - diffX + CharLocation.x;
			float Sylb = listOfCabinet[i].y - 216 - diffY + CharLocation.y;
			float Syub = listOfCabinet[i].y + 216 - diffY + CharLocation.y;

			if ((Sxlb <= CharLocation.x && CharLocation.x <= Sxub) && (Sylb <= CharLocation.y && CharLocation.y <= Syub)) {
				if ((Sxlb <= touchLocation.x && touchLocation.x <= Sxub) && (Sylb <= touchLocation.y && touchLocation.y <= Syub)) {
					inventory->getItem(cabinet->onLoot());
					cabinet->clearCabinet();
				}
			}
		}

		//check closet
		for (int i = 0; i < listOfCloset.size(); i++) {
			float Sxlb = listOfCloset[i].x - 173 - diffX + CharLocation.x;
			float Sxub = listOfCloset[i].x + 173 - diffX + CharLocation.x;
			float Sylb = listOfCloset[i].y - 238 - diffY + CharLocation.y;
			float Syub = listOfCloset[i].y + 238 - diffY + CharLocation.y;

			if ((Sxlb <= CharLocation.x && CharLocation.x <= Sxub) && (Sylb <= CharLocation.y && CharLocation.y <= Syub)) {
				if ((Sxlb <= touchLocation.x && touchLocation.x <= Sxub) && (Sylb <= touchLocation.y && touchLocation.y <= Syub)) {
					mainCharacter->setVisible(false);
					outdoor = false;
				}
			}
		}
	}
	return true;
}

void GameScene::update(float dt) {
	if (runBoost == 1 && staminaDec <= 99.8 && isWalk != 0) {
		staminaDec += 0.2;
		speed = isWalk * (CHARACTER_SPEED + runBoost * RUN_BOOST);
	}
	else {
		speed = isWalk * CHARACTER_SPEED;
		if (staminaDec >= 0.01)
			staminaDec -= 0.05;
	}
	CCLOG("%f", staminaDec);
	StaminaBar->setPercent(100 - staminaDec);
	//mainCharacter->setPosition(Point(mainCharacter->getPositionX() , mainCharacter->getPositionY()));
	backgroundSprite->setPosition(Point(backgroundSprite->getPositionX() - speed, backgroundSprite->getPositionY()));
	diffX += speed;
	//door->fixPosition(door->getPositionX() - speed, door->getPositionY());
	cabinet->fixPosition(cabinet->getPositionX() - speed, cabinet->getPositionY());
	clos->fixPosition(clos->getPositionX() - speed, clos->getPositionY());
}