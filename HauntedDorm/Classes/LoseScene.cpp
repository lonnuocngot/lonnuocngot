#include "LoseScene.h"
#include "Definitions.h"

USING_NS_CC;

Scene* LoseScene::createScene()
{
	return LoseScene::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool LoseScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto backgroundSprite = Label::createWithTTF(StringUtils::format("You Lose"), "fonts/Spooky Halloween.ttf", visibleSize.height * SPLASH_FONT_SIZE);
	backgroundSprite->setColor(Color3B::GREEN);
	backgroundSprite->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	this->addChild(backgroundSprite);

	return true;
}
