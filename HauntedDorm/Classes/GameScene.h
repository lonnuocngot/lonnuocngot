#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class GameScene : public cocos2d::Scene
{
private:
	cocos2d::Sprite *mainCharacter;
	std::vector<cocos2d::Point> listOfDoor, listOfTele, listOfSizeDoor, listOfCabinet, listOfCloset;
	std::vector<int> ChangeY;
	cocos2d::ui::LoadingBar *HeartBar, *StaminaBar;

public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
	void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
	void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);

	bool GameScene::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);

	void Pause_Scene();
	void update(float dt);

    CREATE_FUNC(GameScene);
};

#endif // __GAME_SCENE_H__
