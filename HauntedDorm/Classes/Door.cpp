#include "Door.h"
#include "LoseScene.h"
#include "Definitions.h"

USING_NS_CC;

Door::Door(cocos2d::Scene *scene, float posX, float posY) {
	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();
	
	door1 = Sprite::create("Sprites/Game Scene/Background Entity/Door.png");
	door1->setPosition(Point(posX, posY));

	scene->addChild(door1);
}

void Door::fixPosition(float posX, float posY) {
	door1->setPosition(Point(posX, posY));
}

float Door::getPositionX() {
	return door1->getPositionX();
}

float Door::getPositionY() {
	return door1->getPositionY();
}