#include "Inventory.h"
#include "Definitions.h"
#include "GameScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

std::vector<int> item2;
std::string des_form_JSON = "", name_form_JSON = "";
int childTagPic = 0, childTagNam = 0, childTagDes = 0;

Scene* Inventory::createScene(std::vector<int> item1)
{
	for (int i = 0; i < item1.size(); i++)
		item2.push_back(item1[i]);
	return Inventory::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool Inventory::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//Scroll View List Of Item
	cocos2d::ui::ScrollView *scrollView = cocos2d::ui::ScrollView::create();
	scrollView->setDirection(cocos2d::ui::ScrollView::Direction::VERTICAL);
	scrollView->setContentSize(Size(300, 400));
	scrollView->setInnerContainerSize(Size(1280, 2500));
	scrollView->setBackGroundColor(Color3B::GREEN);
	scrollView->setBounceEnabled(true);
	scrollView->setAnchorPoint(Vec2(0.5, 0.5));
	scrollView->setPosition(Vec2(750, 300));

	auto ItemList = Label::createWithTTF(StringUtils::format("List of Item"), "fonts/Spooky Halloween.ttf", visibleSize.height * SPLASH_FONT_SIZE);
	ItemList->setColor(Color3B::GREEN);
	ItemList->setPosition(Vec2(750, 500 + visibleSize.height * SPLASH_FONT_SIZE));
	this->addChild(ItemList);

	float position = 2500;

	for (int i = 0; i < item2.size(); i++) {
		cocos2d::ui::Button *button;
		if (item2[i] == 1) {
			button = cocos2d::ui::Button::create("Sprites/Game Scene/Inventory Scene/key.png", "Sprites/Game Scene/Inventory Scene/key-onclick.png");
			button->setScale(0.5);
			button->setPosition(Vec2(scrollView->getContentSize().width / 2, position - 40));
			position -= 40 * 2;
			scrollView->addChild(button);
			button->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type) {
				switch (type)
				{
				case ui::Widget::TouchEventType::ENDED:
					des_form_JSON = "key";
					name_form_JSON = "key";
					break;
				default:
					break;
				}

				// Picture
				auto Pic = Sprite::create("Sprites/Game Scene/Inventory Scene/key.png");
				Pic->setPosition(Point(100, 600));

				// Name
				auto Nam = Label::createWithTTF(name_form_JSON, "fonts/Spooky Halloween.ttf", 50);
				Nam->setColor(Color3B::BLUE);
				Nam->setPosition(Point(100, 350));

				// Description
				auto Des = Label::createWithTTF(des_form_JSON, "fonts/Spooky Halloween.ttf", 50);
				Des->setColor(Color3B::BLUE);
				Des->setPosition(Point(100, 150));

				// change children
				Pic->setTag(1);
				Nam->setTag(2);
				Des->setTag(3);
				this->removeChildByTag(childTagPic);
				this->removeChildByTag(childTagNam);
				this->removeChildByTag(childTagDes);
				this->addChild(Pic);
				this->addChild(Nam);
				this->addChild(Des);
				childTagPic = 1;
				childTagNam = 2;
				childTagDes = 3;
			});
		} else 
		if (item2[i] == 2) {
			button = cocos2d::ui::Button::create("Sprites/Game Scene/Inventory Scene/Rice.jpg", "Sprites/Game Scene/Inventory Scene/Rice.jpg");
			button->setScale(0.2);
			button->setPosition(Vec2(scrollView->getContentSize().width / 2, position - 47));
			position -= 47 * 2;
			scrollView->addChild(button);
			button->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type) {
				switch (type)
				{
				case ui::Widget::TouchEventType::ENDED:
					des_form_JSON = "rice";
					name_form_JSON = "rice";
					break;
				default:
					break;
				}

				// Picture
				auto Pic = Sprite::create("Sprites/Game Scene/Inventory Scene/Rice.jpg");
				Pic->setPosition(Point(100, 600));

				// Name
				auto Nam = Label::createWithTTF(name_form_JSON, "fonts/Spooky Halloween.ttf", 50);
				Nam->setColor(Color3B::BLUE);
				Nam->setPosition(Point(100, 350));

				// Description
				auto Des = Label::createWithTTF(des_form_JSON, "fonts/Spooky Halloween.ttf", 50);
				Des->setColor(Color3B::BLUE);
				Des->setPosition(Point(100, 150));

				// change children
				Pic->setTag(4);
				Nam->setTag(5);
				Des->setTag(6);
				this->removeChildByTag(childTagPic);
				this->removeChildByTag(childTagNam);
				this->removeChildByTag(childTagDes);
				this->addChild(Pic);
				this->addChild(Nam);
				this->addChild(Des);
				childTagPic = 4;
				childTagNam = 5;
				childTagDes = 6;
			});
		} else
		if (item2[i] == 3) {
			button = cocos2d::ui::Button::create("Sprites/Game Scene/Inventory Scene/Talisman.jpg", "Sprites/Game Scene/Inventory Scene/Talisman.jpg");
			button->setScale(0.2);
			button->setPosition(Vec2(scrollView->getContentSize().width / 2, position - 43));
			position -= 43 * 2;
			scrollView->addChild(button);
			button->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type) {
				switch (type)
				{
				case ui::Widget::TouchEventType::ENDED:
					des_form_JSON = "talisman";
					name_form_JSON = "talisman";
					break;
				default:
					break;
				}

				// Picture
				auto Pic = Sprite::create("Sprites/Game Scene/Inventory Scene/Talisman.jpg");
				Pic->setPosition(Point(100, 600));

				// Name
				auto Nam = Label::createWithTTF(name_form_JSON, "fonts/Spooky Halloween.ttf", 50);
				Nam->setColor(Color3B::BLUE);
				Nam->setPosition(Point(100, 350));

				// Description
				auto Des = Label::createWithTTF(des_form_JSON, "fonts/Spooky Halloween.ttf", 50);
				Des->setColor(Color3B::BLUE);
				Des->setPosition(Point(100, 150));

				// change children
				Pic->setTag(7);
				Nam->setTag(8);
				Des->setTag(9);
				this->removeChildByTag(childTagPic);
				this->removeChildByTag(childTagNam);
				this->removeChildByTag(childTagDes);
				this->addChild(Pic);
				this->addChild(Nam);
				this->addChild(Des);
				childTagPic = 7;
				childTagNam = 8;
				childTagDes = 9;
			});
		}
	}

	this->addChild(scrollView);

	//Name of Item
	auto Name = Label::createWithTTF(StringUtils::format("Name: "), "fonts/Spooky Halloween.ttf", visibleSize.height * SPLASH_FONT_SIZE);
	Name->setColor(Color3B::GREEN);
	Name->setPosition(Point(100, 400));
	this->addChild(Name);

	//Information Of Item
	auto Description = Label::createWithTTF(StringUtils::format("Description: "), "fonts/Spooky Halloween.ttf", visibleSize.height * SPLASH_FONT_SIZE);
	Description->setColor(Color3B::GREEN);
	Description->setPosition(Point(100, 200));
	this->addChild(Description);

	//Back
	BackButton = Sprite::create("Sprites/Game Scene/Inventory Scene/back.png");
	BackButton->setPosition(Point(visibleSize.width / 3, 100));
	this->addChild(BackButton);

	//Choose
	Choose = Sprite::create("Sprites/Game Scene/Inventory Scene/choose.png");
	Choose->setPosition(Point(visibleSize.width * 2 / 3, 100));
	this->addChild(Choose);

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(Inventory::onTouchBegan, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);

	this->scheduleUpdate();

	return true;
}

bool Inventory::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event) {
	Point touchLocation = touch->getLocation();
	Point BackLocation = BackButton->getPosition();
	Point ChooseLocation = Choose->getPosition();

	if (BackLocation.x - 124 <= touchLocation.x && touchLocation.x <= BackLocation.x + 124) {
		if (BackLocation.y - 34 <= touchLocation.y && touchLocation.y <= BackLocation.y + 34) {
			item.clear();
			Director::getInstance()->popScene();
		}
	}

	if (ChooseLocation.x - 124 <= touchLocation.x && touchLocation.x <= ChooseLocation.x + 124) {
		if (ChooseLocation.y - 34 <= touchLocation.y && touchLocation.y <= ChooseLocation.y + 34) {
			onHand = childTagDes / 3;
			item.clear();
			Director::getInstance()->popScene();
		}
	}

	return true;
}

void Inventory::getItem(std::vector<int> item1) {
	for (int i = 0; i < item1.size(); i++) {
		item.push_back(item1[i]);
	}
}

std::vector<int> Inventory::giveItem() {
	return item;
}

int Inventory::giveHand() {
	return onHand;
}